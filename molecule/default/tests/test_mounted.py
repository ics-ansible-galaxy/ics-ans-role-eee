import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-eee-default')


def test_eee_access(host):
    assert host.file('/opt/epics/modules').is_directory
    assert host.file('/opt/startup/boot').is_directory
    assert host.file('/opt/eldk-5.6/ifc1210').is_directory
