import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-eee-no-dev')


def test_no_epics_env(host):
    assert not host.file('/etc/profile.d/ess_epics_env.sh').exists


def test_no_vdct(host):
    assert not host.file('/opt/VisualDCT').exists


def test_no_dev_packages(host):
    assert not host.package('libusb-devel').is_installed
    assert not host.package('hdf5-devel').is_installed
