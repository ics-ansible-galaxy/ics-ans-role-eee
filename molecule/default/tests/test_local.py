import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-eee-local')


def test_rsync_epics_service_enabled(host):
    service = host.service("rsync-epics.service")
    # Don't check that the service is running because
    # it doesn't run forever
    assert service.is_enabled


def test_rsync_epics_timer_running_and_enabled(host):
    timer = host.service("rsync-epics.timer")
    assert timer.is_running
    assert timer.is_enabled


def test_eee_local_directories(host):
    assert host.file('/opt/epics/modules').is_directory
    assert not host.file('/opt/startup/boot').exists
    assert not host.file('/opt/eldk-5.6/ifc1210').exists
